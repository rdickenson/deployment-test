---
date: 2017-04-14T11:25:05-04:00
description: "Mystery"
tags: []
title: "Chapter VII: Mystery"
disable_share: false
---
I don't think anyone could have known just what was about to happen.
Skies were clear, birds were chirping... It was a beautiful, sunny
day.